package ex2Patro;

import java.util.ArrayList;
import java.util.List;

public class Prova {

    public static void main(String[] args) {
        Camisa camisa1 = new Camisa("Lacoste","XL","Vermell");
        Camisa camisa2 = new Camisa("Springfield","L","Vermell");
        Camisa camisa3 = new Camisa("Pull&Bear","M","Blau");
        Camisa camisa4 = new Camisa("Nike","S","Verd");
        Camisa camisa5 = new Camisa("Tommy Hilfiger","XL","Blau");

        ArrayList<Camisa> camises = new ArrayList<Camisa>();
        camises.add(camisa1);
        camises.add(camisa2);
        camises.add(camisa3);
        camises.add(camisa4);
        camises.add(camisa5);

        printPerCaracteristica(camises, (Camisa c) -> c.getTalla().equals("XL"));
        System.out.println();
        printPerCaracteristica(camises, (Camisa c) -> c.getColor().equals("Vermell"));
        System.out.println();
        printPerCaracteristica(camises, (Camisa c) -> c.getTalla().equals("M") && c.getColor().equals("Blau"));
    }

    private static void printPerCaracteristica(List<Camisa> llista, FiltreCamises filtre){
        for(Camisa camisa: llista){
            if(filtre.camisesPer(camisa)) System.out.println(camisa);
        }
    }
}

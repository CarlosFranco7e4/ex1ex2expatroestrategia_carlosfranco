package ex2Patro;

@FunctionalInterface
interface FiltreCamises {
    boolean camisesPer(Camisa camisa);
}

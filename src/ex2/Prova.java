package ex2;

public class Prova {

    public static void main(String[] args) {
        /*Thread num1 = new Thread(new NumeroPrimerThread(17));
        Thread num2 = new Thread(new NumeroPrimerThread(22));
        Thread num3 = new Thread(new NumeroPrimerThread(61));
        num1.start();
        num2.start();
        num3.start();*/

        Thread num1 = new Thread(new NumeroPrimerRunnable(17));
        Thread num2 = new Thread(new NumeroPrimerRunnable(22));
        Thread num3 = new Thread(new NumeroPrimerRunnable(61));
        num1.start();
        num2.start();
        num3.start();
    }
}

package ex2;

public class NumeroPrimerThread extends Thread{

    private long numero;

    public NumeroPrimerThread(long num){
        numero = num;
    }

    private boolean esPrimer(long numero){
        if (numero == 0 || numero == 1 || numero == 2 || numero == 3 || numero == 5){
            return true;
        } else return numero % 2 != 0 && numero % 3 != 0 && numero % 5 != 0 && numero % 7 != 0;
    }

    @Override
    public void run(){
        esPrimer(numero);
        long numInicial = numero;
        if (esPrimer(numero)){
            numero++;
            while (!esPrimer(numero)){
                numero++;
            }
            System.out.println(Thread.currentThread().getName()+": El següent número primer al número "+numInicial+" és "+numero+".");
        } else {
            System.out.println(Thread.currentThread().getName()+": El número "+numero+" no és primer.");
        }
    }
}

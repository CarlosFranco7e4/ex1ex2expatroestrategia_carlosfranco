package ex1;

public class Prova {

    public static void main(String[] args) {
        /*Thread fitxer1 = new Thread(new ex1.LlegirFitxersThread("fitxer1.txt"));
        Thread fitxer2 = new Thread(new ex1.LlegirFitxersThread("fitxer2.txt"));
        Thread fitxer3 = new Thread(new ex1.LlegirFitxersThread("fitxer3.txt"));
        fitxer1.start();
        fitxer2.start();
        fitxer3.start();*/

        Thread fitxer1 = new Thread(new LlegirFitxersRunnable("fitxer1.txt"));
        Thread fitxer2 = new Thread(new LlegirFitxersRunnable("fitxer2.txt"));
        Thread fitxer3 = new Thread(new LlegirFitxersRunnable("fitxer3.txt"));
        fitxer1.start();
        fitxer2.start();
        fitxer3.start();
    }
}

package ex1;

import java.io.*;

public class LlegirFitxersThread extends Thread {
    private String fitxer;

    public LlegirFitxersThread(String nomFitxer){
        fitxer = nomFitxer;
    }

    private void llegirFitxer(String fitxer){
        try {
            BufferedReader br = new BufferedReader(new FileReader(fitxer));
            String linea;
            int linies = 0;
            while ((linea = br.readLine()) != null){
                linies++;
            }
            System.out.println("El fitxer "+fitxer+": "+Thread.currentThread().getName()+" té "+linies+" línies.");
        } catch (IOException e) {
            System.out.println("Error: "+e);
        }
    }

    @Override
    public void run(){
        llegirFitxer(fitxer);
    }
}
